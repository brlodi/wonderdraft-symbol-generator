# Wonderdraft Symbol Generator

This was an early attempt at automatically processing input SVGs for use as stampable symbols in [Wonderdraft](https://www.wonderdraft.net), created as a favor for a fellow member of the Wonderdraft Discord community. It ended up not producing great results, especially when trying to generalize to different SVG inputs than the samples.

The code is preserved here primarily for posterity. I wouldn't recommend using it.
