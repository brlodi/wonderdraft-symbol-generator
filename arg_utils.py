import argparse
import logging
import os


def parse_args():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('source_directory')
    arg_parser.add_argument('output_directory',
                            nargs='?',
                            default='output')
    arg_parser.add_argument('-g', '--grouped',
                            action='store_true',
                            help='Generate symbols into folders based on their names, as needed for tree or mountain symbols')
    arg_parser.add_argument('-1', '--single-color',
                            action='store_true',
                            help='Generate only a single image per color scheme, instead of one for every possible combination of the scheme\'s colors')
    arg_parser.add_argument('--color-scheme-file',
                            type=open,
                            default='./color_schemes.txt',
                            help='Specifies an alternative file to load color schemes from')
    arg_parser.add_argument('--log-level',
                            choices=['DEBUG', 'INFO',
                                     'WARNING', 'ERROR', 'CRITICAL'],
                            default='WARNING')
    arg_parser.add_argument('-c', '--template-colors',
                            nargs='+',
                            default=['cf211b', '64c93f', '4b90e5'])
    return arg_parser.parse_args()


def handle_args(args):
    log_level = getattr(logging, args.log_level.strip().upper(), None)
    logging.basicConfig(level=log_level)

    logging.debug(args)

    args.source_directory = resolve_input_dir(args.source_directory)
    lazy_make_dir(args.output_directory)


def resolve_input_dir(input_path):
    rpath = os.path.join(os.getcwd(), input_path)
    if not os.path.exists(rpath):
        raise FileNotFoundError()
    if not os.path.isdir(rpath):
        raise IOError(f'{rpath} is a normal file, not a directory')
    if not [f for f in os.listdir(rpath) if 'svg' in f.lower()]:
        logging.warning(f'Input directory {rpath} contains no SVG files!')
        quit()
    return rpath


def lazy_make_dir(path):
    rpath = os.path.join(os.getcwd(), path)
    if os.path.isfile(rpath):
        raise FileExistsError()
    if not os.path.exists(rpath):
        os.mkdir(rpath)
