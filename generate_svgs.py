import itertools
import os
import re


COLOR_SCHEME_FILE = './color_schemes.txt'
SVG_TEMPLATE_DIR = './templates'
SVG_OUTPUT_DIR = './svg_output'
TEMPLATE_COLORS = ['cf211b', '64c93f', '4b90e5']


def process_template_name(filename):
    match = re.match(r'(.+) Master (\d+)\.svg', fn, flags=re.IGNORECASE)
    design_name = match.group(1)
    design_num = match.group(2)
    return (design_name, design_num)


# Ensure the output directory exists
if not (os.path.exists(SVG_OUTPUT_DIR) and os.path.isdir(SVG_OUTPUT_DIR)):
    os.mkdir(SVG_OUTPUT_DIR)

color_schemes = [
    {'name': s[0], 'variant': s[1], 'colors': s[2:]}
    for s in [
        l.strip().split(',')
        for l in open(COLOR_SCHEME_FILE).readlines()
        if l.strip()[0] != '#'
    ]
]

template_filenames = sorted([
    fn
    for fn in os.listdir(SVG_TEMPLATE_DIR)
    if os.path.isfile(os.path.join(SVG_TEMPLATE_DIR, fn)) and '.svg' in fn
])

# For each of the template files
for fn in template_filenames:
    # Extract the name and number of the design we're operating on
    design_name, design_num = process_template_name(fn)

    # Read in the master SVG
    with open(os.path.join(SVG_TEMPLATE_DIR, fn)) as f:
        svg_in = f.read()

    # Find out which color slots are in the template
    slot_colors = [t for t in TEMPLATE_COLORS if t in svg_in]
    num_colors = len(slot_colors)

    # For each color scheme defined in the color scheme file
    for s in color_schemes:
        scheme_name = s['name']
        variant = s['variant']

        # Generate all of the applicable permutations of the current color scheme
        # (n=3 for one slot, n=6 for two or three slots)
        color_permutations = list(
            itertools.permutations(s['colors'], num_colors))

        p_num = 1

        # Generate a new SVG for each permutation
        for perm in color_permutations:
            svg_out = svg_in
            for (t, c) in zip(slot_colors, perm):
                svg_out = svg_out.replace(t, c)

            fn_out = f'{design_name} {scheme_name} {design_num}{variant}{p_num}.svg'

            with open(os.path.join(SVG_OUTPUT_DIR, fn_out), 'w') as of:
                of.write(svg_out)

            p_num += 1
