import copy
import logging


def get_color_schemes(file):
    # Remove comment lines and parse CSV into list of lists
    lines = [l.strip() for l in file.readlines() if l.strip()[0] != '#']
    scheme_lists = [l.split(',') for l in lines]

    # Warn on empty schemes and remove them
    for line in [l for l in scheme_lists if len(l) < 2]:
        logging.warning(
            f'{line} is not a valid color scheme definition. Skipping!')
        scheme_lists.remove(line)

    # Parse each scheme-list into dictionary
    schemes = [scheme_list2dict(l) for l in scheme_lists]
    return schemes


def ensure_color_count(scheme, min_colors):
    out_scheme = copy.deepcopy(scheme)
    if len(scheme['colors']) < min_colors:
        out_scheme['colors'] = extend_by_repetition(
            scheme['colors'], min_colors)
    return out_scheme


def scheme_list2dict(scheme_list):
    name = scheme_list[0]
    colors = [c.replace('#', '') for c in scheme_list[1:]]
    return {'name': name, 'colors': colors}


def extend_by_repetition(a_list, target_length):
    new_list = a_list.copy()
    while len(new_list) < target_length:
        new_list.extend(a_list)
    return new_list[:target_length]


def get_slot_count(template, slot_colors):
    return len([c for c in slot_colors if f'#{c}' in template.lower()])
