import arg_utils
import itertools
import logging
import os
import re
import wd_color

args = arg_utils.parse_args()
arg_utils.handle_args(args)

pattern_template_name = re.compile(r'(.+?)\s?(\d+[a-z]?)?\.svg', flags=re.I)

schemes = wd_color.get_color_schemes(args.color_scheme_file)
template_filenames = [fn for fn in os.listdir(args.source_directory)
                      if 'svg' in fn.lower()]

for fn in sorted(template_filenames):
    with open(os.path.join(args.source_directory, fn)) as f:
        template = f.read()
    slot_count = wd_color.get_slot_count(template, args.template_colors)

    print(pattern_template_name.match(fn).group(0, 1, 2))

    # for scheme in schemes:
    #     scheme = wd_color.ensure_color_count(scheme, slot_count)

    #     if args.grouped:
    #         wd_convert.gen_grouped(template, scheme, args.output_directory,
    #                                pattern_template_name)
    #     else:
    #         wd_convert.gen_single(template, scheme, args.output_directory)

    #     scheme_perms = list(itertools.permutations(
    #         scheme['colors'], slot_count))

    #     for num, colors in enumerate(scheme_perms, 1):
